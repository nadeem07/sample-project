<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<style type="text/css">
		body {
 			 background-image: url(veg2.jpg);
 			 background-size: cover;
 			 background-attachment: fixed;
			}
			button {background-color: #e7e7e7; 
					color: black;
					border: none;
					padding: 15px 32px;
					text-align: center;
					text-decoration: none;
					display: inline-block;
					font-size: 16px;
			}

</style>

<title>Vendor menu</title>
</head>
<body> 

<h1 align=center>MENU CHART</h1>
<form action="" method="post">

		<table align="center">
			<tr>
			<th>Days</th>
				<th>Vegetable 1</th>
				<th>Vegetable 2</th>
				<th>Roti</th>
				<th>Rice</th>
				<th>Salad</th>
			</tr>
			<tr>
			<th>MONDAY</th>
				<td><input type="text" name="mon_vegetable1"></td>
				<td><input type="text" name="mon_vegetable2"></td>
				<td><input type="checkbox" name="mon_roti" value="mon_roti"></td>
				<td><input type="checkbox" name="mon_rice" value="mon_rice"></td>
				<td><input type="checkbox" name="mon_salad" value="mon_salad"></td>
			</tr>
			<tr>
			<th>TUESDAY</th>
				<td><input type="text" name="tue_vegetable1"></td>
				<td><input type="text" name="tue_vegetable2"></td>
				<td><input type="checkbox" name="tue_roti" value="tue_roti"></td>
				<td><input type="checkbox" name="tue_rice" value="tue_rice"></td>
				<td><input type="checkbox" name="tue_salad" value="tue_salad"></td>
			</tr>
			<tr>
			<th>WEDNESDAY</th>
				<td><input type="text" name="wed_vegetable1"></td>
				<td><input type="text" name="wed_vegetable2"></td>
				<td><input type="checkbox" name="wed_roti" value="wed_roti"></td>
				<td><input type="checkbox" name="wed_rice" value="wed_rice"></td>
				<td><input type="checkbox" name="wed_salad" value="wed_salad"></td>
			</tr>
			<tr>
			<th>THURSDAY</th>
				<td><input type="text" name="thu_vegetable1"></td>
				<td><input type="text" name="thu_vegetable2"></td>
				<td><input type="checkbox" name="thu_roti" value="thu_roti"></td>
				<td><input type="checkbox" name="thu_rice" value="thu_rice"></td>
				<td><input type="checkbox" name="thu_salad" value="thu_salad"></td>
			</tr>
			<tr>
			<th>FRIDAY</th>
				<td><input type="text" name="fri_vegetable1"></td>
				<td><input type="text" name="fri_vegetable2"></td>
				<td><input type="checkbox" name="fri_roti" value="fri_roti"></td>
				<td><input type="checkbox" name="fri_rice" value="fri_rice"></td>
				<td><input type="checkbox" name="fri_salad" value="fri_salad"></td>
			</tr>
		</table>
		<br>
		<center><button type="button">Submit</button>
		</center>

</body>
</html>